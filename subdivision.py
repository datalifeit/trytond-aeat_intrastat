# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pyson import Eval, Id
from trytond.pool import Pool


class IntrastatSubdivision(ModelSQL, ModelView):
    """Intrastat Subdivision distance"""
    __name__ = 'intrastat.subdivision_distance'

    subdivision = fields.Many2One('country.subdivision', 'Subdivision',
        required=True, select=True)
    distance = fields.Float('Distance', required=True,
        digits=(16, Eval('distance_uom_digits', 2)),
        domain=[('distance', '>=', 0)],
        depends=['distance_uom_digits'])
    distance_uom = fields.Many2One('product.uom', 'Distance Uom',
        required=True,
        domain=[('category', '=', Id('product', 'uom_cat_length'))])
    distance_uom_digits = fields.Function(
        fields.Integer('Distance Uom Digits'),
        'on_change_with_distance_uom_digits')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('subdivision_uniq', Unique(t, t.subdivision),
                'Subidivion must be unique.'),
        ]

    @classmethod
    def default_distance_uom(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        return ModelData.get_id('product', 'uom_kilometer')

    @staticmethod
    def default_distance_uom_digits():
        return 2

    @fields.depends('distance_uom', methods=['default_distance_uom_digits'])
    def on_change_with_distance_uom_digits(self, name=None):
        if self.distance_uom:
            return self.distance_uom.digits
        return self.default_distance_uom_digits()

    @classmethod
    def _get_distances(cls, from_subdivision, to_subdivision):
        pool = Pool()
        Uom = pool.get('product.uom')

        distances = cls.search([
            ('subdivision', 'in', [from_subdivision, to_subdivision])])

        if not distances:
            return (0.0, 0.0)
        distance1 = [d for d in distances if d.subdivision == from_subdivision]
        distance2 = [d for d in distances if d.subdivision == to_subdivision]
        base_uom = None
        if not distance1:
            distance1 = 0.0
        else:
            distance1, = distance1
            base_uom = distance1.distance_uom
            distance1 = distance1.distance
        if not distance2:
            distance2 = 0.0
        else:
            distance2, = distance2
            if base_uom:
                distance2 = Uom.compute_qty(distance2.distance_uom,
                    distance2.distance, base_uom)
            else:
                distance2 = distance2.distance
        return (distance1, distance2)
