# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import declaration
from . import subdivision
from . import invoice
from . import aeat_349


def register():
    Pool.register(
        declaration.IntrastatDeclarationLine,
        declaration.IntrastatDeclarationSummary,
        declaration.IntrastatDeclaration,
        subdivision.IntrastatSubdivision,
        invoice.Line,
        module='aeat_intrastat', type_='model')
    Pool.register(
        invoice.Line2,
        module='aeat_intrastat', type_='model',
        depends=['carrier_load', 'sale_cost'])
    Pool.register(
        aeat_349.IntrastatDeclarationLine349,
        aeat_349.Declaration349ComparativeContext,
        aeat_349.Declaration349Comparative,
        module='aeat_intrastat', type_='model',
        depends=['aeat_349'])
