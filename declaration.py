# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import os
import csv
import tempfile
import stdnum.eu.vat as vat
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Bool
from trytond.model import ModelView, fields
from trytond.modules.intrastat.intrastat import STATES
from sql import Null
from sql.aggregate import Sum
from sql.conditionals import Case


def format_number(value, digits=(12, 3)):
    if value is None:
        value = 0.0
    f = '{:' + str(digits[0]) + '.' + str(digits[1]) + 'f}'
    return f.format(value).replace('.', ',').strip(' ')


class IntrastatDeclaration(metaclass=PoolMeta):
    __name__ = 'intrastat.declaration'

    dispatch_message = fields.Text('Dispatch message', readonly=True)
    arrival_message = fields.Text('Arrival message', readonly=True)

    @classmethod
    def __setup__(cls):
        super(IntrastatDeclaration, cls).__setup__()
        cls._buttons.update({
            'create_file': {
                'invisible': (~Eval('state').in_(['draft', 'processed'])
                    | ~Bool(Eval('lines'))),
                'depends': ['state', 'lines'],
            }
        })

    @classmethod
    def do(cls, records):
        super(IntrastatDeclaration, cls).do(records)
        cls.create_file(records)

    @classmethod
    @ModelView.button
    def create_file(cls, declarations):
        pool = Pool()
        Zip = pool.get('country.postal_code')

        _, arrival_file_name = tempfile.mkstemp('.csv', 'intrastat_')
        _, dispatch_file_name = tempfile.mkstemp('.csv', 'intrastat_')

        subdivision2postal_code = {}
        for declaration in declarations:
            with open(arrival_file_name, 'w+') as arrival_file, open(
                    dispatch_file_name, 'w+') as dispatch_file:
                arrival_spamwriter = csv.writer(arrival_file, delimiter=';')
                dispatch_spamwriter = csv.writer(dispatch_file, delimiter=';')
                for summary in declaration.summaries:
                    country = (summary.destination_country
                        if summary.type != 'arrival'
                        else summary.origin_country)
                    subdivision = summary.destination_subdivision
                    postal_code = subdivision2postal_code.get(subdivision)
                    if postal_code is None:
                        postal_code, = (
                            Zip.search([
                                ('subdivision.type', '=', 'province'),
                                ['OR',
                                    ('subdivision', '=', subdivision),
                                    ('subdivision.parent', '=', subdivision)]
                                ], limit=1) or [''])
                    subdivision2postal_code.setdefault(subdivision,
                        postal_code)
                    try:
                        if summary.type == 'dispatch':
                            if not summary.party_tax_identifier:
                                identifier_code = ''
                            elif summary.party_tax_identifier_vies_valid:
                                identifier_code = \
                                    summary.party_tax_identifier.code
                            else:
                                identifier_code = 'QV999999999999'
                        else:
                            identifier_code = ''
                    except Exception:
                        identifier_code = 'QV999999999999'
                    row = [
                        country.code if country else '',
                        postal_code.postal_code[:2] if postal_code else '',
                        (summary.incoterm.abbreviation
                            if summary.incoterm else ''),
                        (summary.transaction_.code
                            if summary.transaction_ else ''),
                        (summary.transport_mode.code
                            if summary.transport_mode else ''),
                        '',
                        (summary.intrastat_code.trimmed_code
                            if summary.intrastat_code else ''),
                        (summary.origin_country.code
                            if summary.origin_country else ''),
                        '',
                        format_number(summary.weight),
                        format_number(summary.quantity),
                        format_number(summary.amount or 1, digits=(13, 2)),
                        format_number(summary.statistic_value or 1,
                            digits=(13, 2))
                    ]
                    if summary.type == 'arrival':
                        arrival_spamwriter.writerow(row)
                    elif summary.type == 'dispatch':
                        row.append(identifier_code)
                        dispatch_spamwriter.writerow(row)
                arrival_file.seek(0)
                dispatch_file.seek(0)
                declaration.arrival_message = arrival_file.read()
                declaration.dispatch_message = dispatch_file.read()
                declaration.save()
            os.remove(arrival_file_name)
            os.remove(dispatch_file_name)

    def _get_lines(self, invoice_lines, conf):
        lines = super()._get_lines(invoice_lines, conf)
        code2valid = {}
        for line in lines:
            if (line.invoice_line.product
                    and line.invoice_line.product.type == 'goods'):
                line.statistic_value = \
                    line.invoice_line.intrastat_statistic_value
            else:
                line.statistic_value = line.amount
            tax_identifier = line.invoice_line.invoice.party_tax_identifier
            line.party_tax_identifier = tax_identifier

            # Not validate if it's not necessary.
            if line.type != 'dispatch':
                continue
            if not tax_identifier:
                continue

            tax_identifier_valid = code2valid.get(tax_identifier.code)
            if tax_identifier_valid is None:
                try:
                    if vat.check_vies(tax_identifier.code)['valid']:
                        tax_identifier_valid = True
                    else:
                        tax_identifier_valid = False
                except Exception:
                    tax_identifier_valid = False
                code2valid[tax_identifier.code] = tax_identifier_valid
            line.party_tax_identifier_vies_valid = tax_identifier_valid
        return lines


class IntrastatDeclarationLine(metaclass=PoolMeta):
    __name__ = 'intrastat.declaration.line'

    statistic_value = fields.Numeric('Statistic Value',
        digits=(16, Eval('_parent_declaration', {}).get('currency_digits', 2)),
        states=STATES)
    party_tax_identifier = fields.Many2One('party.identifier',
        'Party tax identifier', select=True,
        states=STATES)
    party_tax_identifier_vies_valid = fields.Boolean(
        'Party tax identifier VIES valid', readonly=True, states=STATES)


class IntrastatDeclarationSummary(metaclass=PoolMeta):
    __name__ = 'intrastat.declaration.summary'

    statistic_value = fields.Numeric('Statistic Value',
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    party_tax_identifier = fields.Many2One('party.identifier',
        'Party tax identifier', readonly=True)
    party_tax_identifier_vies_valid = fields.Boolean(
        'Party tax identifier VIES valid', readonly=True)

    @classmethod
    def _get_columns(cls, line):
        res = super(IntrastatDeclarationSummary, cls)._get_columns(line) + [
            Sum(line.statistic_value).as_('statistic_value'),
            Case((line.type == 'dispatch', line.party_tax_identifier),
                else_=Null).as_('party_tax_identifier'),
            Case((line.type == 'dispatch',
                line.party_tax_identifier_vies_valid),
                else_=Null).as_('party_tax_identifier_vies_valid'),
        ]
        res[5] = Case((line.type == 'arrival', line.destination_subdivision),
            else_=line.origin_subdivision).as_('destination_subdivision')
        return res

    @classmethod
    def _get_group_columns(cls, line):
        res = super(IntrastatDeclarationSummary, cls)._get_group_columns(line)
        res[5] = Case((line.type == 'arrival', line.destination_subdivision),
            else_=line.origin_subdivision)
        res.append(Case(
            (line.type == 'dispatch', line.party_tax_identifier), else_=Null))
        res.append(Case(
            (line.type == 'dispatch', line.party_tax_identifier_vies_valid),
            else_=Null))
        return res
