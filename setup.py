#!/usr/bin/env python3
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from setuptools import setup, find_packages
import re
import os
import io
from configparser import ConfigParser

MODULE2PREFIX = {
    'intrastat': 'datalife',
}


def read(fname):
    return io.open(
        os.path.join(os.path.dirname(__file__), fname),
        'r', encoding='utf-8').read()

def get_require_version(name):
    if minor_version % 2:
        require = '%s >= %s.%s.dev0, < %s.%s'
    else:
        require = '%s >= %s.%s, < %s.%s'
    require %= (name, major_version, minor_version,
        major_version, minor_version + 1)
    return require

config = ConfigParser()
config.readfp(open('tryton.cfg'))
info = dict(config.items('tryton'))
for key in ('depends', 'extras_depend', 'xml'):
    if key in info:
        info[key] = info[key].strip().splitlines()
version = info.get('version', '0.0.1')
major_version, minor_version, _ = version.split('.', 2)
major_version = int(major_version)
minor_version = int(minor_version)
name = 'datalife_aeat_intrastat'
download_url = 'https://gitlab.com/datalifeit/trytond-aeat_intrastat'

series = '%s.%s' % (major_version, minor_version)
if minor_version % 2:
    branch = 'default'
else:
    branch = series

dependency_links = {
    'intrastat':
        'git+https://gitlab.com/datalifeit/'
        'trytond-intrastat@%(branch)s'
        '#egg=datalife_intrastat' % {
            'branch': branch,
            },
    'sale_cost':
        'git+https://gitlab.com/datalifeit/'
        'trytond-sale_cost@%(branch)s'
        '#egg=datalife_sale_cost-%(series)s' % {
            'branch': branch,
            'series': series
            },
    'carrier_load':
        'git+https://gitlab.com/datalifeit/'
        'trytond-carrier_load@%(branch)s'
        '#egg=datalife_carrier_load-%(series)s' % {
            'branch': branch,
            'series': series
            }
}

requires = ['python-stdnum', 'zeep']
for dep in info.get('depends', []):
    if not re.match(r'(ir|res)(\W|$)', dep):
        prefix = MODULE2PREFIX.get(dep, 'trytond')
        req = get_require_version('%s_%s' % (prefix, dep))
        if dep in dependency_links:
            req = '%s_%s @ %s' % (prefix, dep, dependency_links[dep])
        requires.append(req)
requires.append(get_require_version('trytond'))

tests_require = [
    get_require_version('proteus'),
    get_require_version('datalife_sale_cost'),
    get_require_version('datalife_carrier_load')
    ]

dependency_links = list(dependency_links.values())
if minor_version % 2:
    dependency_links.append('https://trydevpi.tryton.org/')

setup(name=name,
    version=version,
    description='Tryton Aeat Intrastat Module',
    long_description=read('README.md'),
    author='Datalife',
    author_email='info@datalifeit.es',
    url='https://gitlab.com/datalifeit/',
    download_url=download_url,
    keywords='',
    package_dir={'trytond.modules.aeat_intrastat': '.'},
    packages=(
        ['trytond.modules.aeat_intrastat'] +
        ['trytond.modules.aeat_intrastat.%s' % p for p in find_packages()]
    ),
    package_data={
        'trytond.modules.aeat_intrastat': (info.get('xml', [])
            + ['tryton.cfg', 'view/*.xml', 'locale/*.po', '*.fodt',
                'icons/*.svg', 'tests/*.rst']),
        },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Plugins',
        'Framework :: Tryton',
        'Intended Audience :: Developers',
        'Intended Audience :: Financial and Insurance Industry',
        'Intended Audience :: Legal Industry',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Natural Language :: Bulgarian',
        'Natural Language :: Catalan',
        'Natural Language :: Czech',
        'Natural Language :: Dutch',
        'Natural Language :: English',
        'Natural Language :: French',
        'Natural Language :: German',
        'Natural Language :: Hungarian',
        'Natural Language :: Italian',
        'Natural Language :: Portuguese (Brazilian)',
        'Natural Language :: Russian',
        'Natural Language :: Slovenian',
        'Natural Language :: Spanish',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Topic :: Office/Business',
        ],
    license='GPL-3',
    phyton_requires='>=3.4',
    install_requires=requires,
    dependency_links=dependency_links,
    zip_safe=False,
    entry_points="""
    [trytond.modules]
    aeat_intrastat = trytond.modules.aeat_intrastat
    """,
    )
