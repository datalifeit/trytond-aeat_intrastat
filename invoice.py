# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from trytond.pool import PoolMeta, Pool


class Line(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @property
    def intrastat_statistic_value(self):
        return self.intrastat_amount


class Line2(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @property
    def intrastat_statistic_value(self):
        if (self.intrastat_type == 'arrival'
                and 'EXW' in [i.rule.abbreviation
                for i in self.invoice.incoterms]):
            return Decimal('0.0')
        pool = Pool()
        SaleLine = pool.get('sale.line')
        if (self.intrastat_type == 'dispatch'
                and self.origin
                and isinstance(self.origin, SaleLine)):
            IntrastatSubdivision = pool.get('intrastat.subdivision_distance')

            amount = self.intrastat_amount
            if 'EXW' in [i.rule.abbreviation for i in self.invoice.incoterms]:
                return amount

            # Get carrier amount
            cost_lines = [cl for cl in self.origin.distributed_costs
                if 'carrier_amount' in cl.cost.formula]
            # TODO: apply factor when sale line invoiced in many lines
            carrier_amount = self.invoice.company.currency.round(sum(
                line.amount or Decimal('0.0') for line in cost_lines))
            # Get subdivisions distance
            sub1, sub2 = [a.subdivision if a else None
                for a in self.intrastat_addresses]
            dist1, dist2 = IntrastatSubdivision._get_distances(sub1, sub2)
            if not dist1 and not dist2:
                return amount
            return self.invoice.company.currency.round(amount - (
                carrier_amount * Decimal(dist1 / (dist1 + dist2))))
        return self.amount
