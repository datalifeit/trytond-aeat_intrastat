datalife_aeat_intrastat
=======================

The aeat_intrastat module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-aeat_intrastat/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-aeat_intrastat)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
