=======================
Aeat Intrastat Scenario
=======================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.intrastat.tests.tools import configure_intrastat
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> import datetime
    >>> from decimal import Decimal
    >>> from dateutil.relativedelta import relativedelta
    >>> today = datetime.date.today()

Install aeat_intrastat::

    >>> config = activate_modules('aeat_intrastat')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> cash = accounts['cash']

    >>> Journal = Model.get('account.journal')
    >>> PaymentMethod = Model.get('account.invoice.payment.method')
    >>> cash_journal, = Journal.find([('type', '=', 'cash')])
    >>> cash_journal.save()
    >>> payment_method = PaymentMethod()
    >>> payment_method.name = 'Cash'
    >>> payment_method.journal = cash_journal
    >>> payment_method.credit_account = cash
    >>> payment_method.debit_account = cash
    >>> payment_method.save()

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create parties::

    >>> Subdivision = Model.get('country.subdivision')
    >>> Country = Model.get('country.country')
    >>> Party = Model.get('party.party')
    >>> CountryZip = Model.get('country.postal_code')
    >>> SubdivisionType = Model.get('party.address.subdivision_type')
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> country_es = Country(name="Spain", code="ES", eu_member=True)
    >>> country_es.save()
    >>> subdivision_type, = SubdivisionType.find([('country_code', '=', 'ES')])
    >>> subdivision_type.types = list(subdivision_type.types) + ['autonomous community']
    >>> subdivision_type.save()
    >>> rgmurcia = Subdivision(
    ...     name="Región de Murcia", code="ES-MC", type='autonomous community', country=country_es)
    >>> rgmurcia.save()
    >>> murcia = Subdivision(
    ...     name="Murcia", code="ES-MU", type='province', country=country_es, parent=rgmurcia)
    >>> murcia.save()
    >>> address_es = customer.addresses.new()
    >>> address_es.name = 'Address 2'
    >>> address_es.country = country_es
    >>> address_es.subdivision = rgmurcia
    >>> address_es.save()
    >>> customer.save()
    >>> country_zip = CountryZip(postal_code='30000', subdivision=murcia, country=country_es)
    >>> country_zip.save()
    >>> company_addr = company.party.addresses[0]
    >>> company_addr.country = country_es
    >>> company_addr.subdivision = rgmurcia
    >>> company_addr.save()

    >>> Lang = Model.get('ir.lang')
    >>> it_lang, = Lang.find([('code', '=', 'it')], limit=1)
    >>> customer_it = Party(name='Customer IT')
    >>> customer_it.lang = it_lang
    >>> tax_identifier = customer_it.identifiers.new()
    >>> tax_identifier.type = 'eu_vat'
    >>> tax_identifier.code = 'IT00972990709'
    >>> customer_it.save()
    >>> subdivision_type, = SubdivisionType.find([('country_code', '=', 'IT')])
    >>> subdivision_type.types = list(subdivision_type.types) + ['autonomous community']
    >>> subdivision_type.save()
    >>> country_it = Country(name="Italy", code="IT", eu_member=True)
    >>> country_it.save()
    >>> roma = Subdivision(
    ...     name="Roma", code="IT-RO", type='autonomous community', country=country_it)
    >>> roma.save()
    >>> address_it = customer_it.addresses[0]
    >>> address_it.name = 'Address IT'
    >>> address_it.country = country_it
    >>> address_it.subdivision = roma
    >>> address_it.save()

Create supplier::

    >>> country_de = Country(name="Germany", code="DE", eu_member=True)
    >>> country_de.save()
    >>> berlin = Subdivision(
    ...     name="Berlin", code="DE-BE", type='state', country=country_de)
    >>> berlin.save()
    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier', active=True)
    >>> supplier.save()
    >>> address = supplier.addresses[0]
    >>> address.country = country_de
    >>> address.subdivision = berlin
    >>> address.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

Configure Intrastat::

    >>> conf = configure_intrastat()
    >>> conf.declare_arrival = True
    >>> conf.declare_dispatch = True
    >>> conf.save()
    >>> TransactionType = Model.get('intrastat.transaction.type')
    >>> type11, = TransactionType.find([('code', '=', '11')])
    >>> conf.transaction_type = type11
    >>> conf.save()

Create Intrastat codes::

    >>> Code = Model.get('intrastat.code')
    >>> code_c1 = Code(code='04', description="DAIRY PRODUCE; BIRDS' EGGS; NATURAL HONEY; EDIBLE PRODUCTS OF ANIMAL ORIGIN,  NOT ELSEWHERE SPECIFIED OR INCLUDED")
    >>> code_c1.save()
    >>> code_g1 = Code(code='0401', description='Milk and cream', parent=code_c1)
    >>> code_g1.save()
    >>> code_p1 = Code(code='0401 10', description='Of a fat content, by weight, not exceeding 1 %', parent=code_g1)
    >>> code_p1.save()
    >>> code_c1 = Code(code='0401 10 10', description='In immediate packings of a net content not exceeding two litres', parent=code_p1)
    >>> code_c1.save()

Create Intrastat Declaration::

    >>> Declaration = Model.get('intrastat.declaration')
    >>> declaration = Declaration(from_date=today,to_date=today, company=company, description='Declaration 1')
    >>> declaration.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')

    >>> unit, = ProductUom.find([('name', '=', 'Kilogram')])
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.salable = True
    >>> template.list_price = Decimal('30')
    >>> template.account_category = account_category
    >>> template.intrastat_code = code_c1
    >>> template.save()
    >>> product, = template.products

Create invoices::

    >>> Invoice = Model.get('account.invoice')
    >>> invoice = Invoice(type='in')
    >>> invoice.party = supplier
    >>> invoice.invoice_date = today
    >>> invoice.accounting_date = today
    >>> line = invoice.lines.new()
    >>> line.product = product
    >>> line.quantity = 2
    >>> line.unit_price = Decimal('10')
    >>> invoice.save()
    >>> invoice.click('post')

    >>> invoice2 = Invoice(type='out')
    >>> invoice2.party = customer_it
    >>> invoice2.party_tax_identifier != None
    True
    >>> invoice2.invoice_date = today
    >>> invoice2.accounting_date = today
    >>> line = invoice2.lines.new()
    >>> line.product = product
    >>> line.quantity = 2
    >>> line.unit_price = Decimal('10')
    >>> invoice2.save()
    >>> invoice2.click('post')

Check statistic_value from IntrastatLine::

    >>> declaration.click('create_lines')
    >>> line, = [l for l in declaration.lines if l.type == 'arrival']
    >>> line2, = [l for l in declaration.lines if l.type == 'dispatch']
    >>> line.statistic_value
    Decimal('20.00')
    >>> line2.party_tax_identifier.code
    'IT00972990709'

Check summary::

    >>> declaration.click('create_file')
    >>> declaration.arrival_message
    'DE;30;;11;1;;04011010;DE;;2,000;2,000;20,00;20,00\n'
    >>> declaration.dispatch_message
    'IT;30;;11;1;;04011010;ES;;2,000;2,000;20,00;20,00;IT00972990709\n'
