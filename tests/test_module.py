# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class AeatIntrastatTestCase(ModuleTestCase):
    """Test Aeat Intrastat module"""
    module = 'aeat_intrastat'
    extras = ['aeat_349', 'carrier_load', 'sale_cost']


del ModuleTestCase
