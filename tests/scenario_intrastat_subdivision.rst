==============================
Intrastat Subdivision Scenario
==============================

Imports::

    >>> import datetime
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.intrastat.tests.tools import configure_intrastat
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> today = datetime.date.today()


Install aeat_intrastat::

    >>> config = activate_modules(['aeat_intrastat', 'carrier_load', 'sale_cost'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> cash = accounts['cash']

    >>> Journal = Model.get('account.journal')
    >>> PaymentMethod = Model.get('account.invoice.payment.method')
    >>> cash_journal, = Journal.find([('type', '=', 'cash')])
    >>> cash_journal.save()
    >>> payment_method = PaymentMethod()
    >>> payment_method.name = 'Cash'
    >>> payment_method.journal = cash_journal
    >>> payment_method.credit_account = cash
    >>> payment_method.debit_account = cash
    >>> payment_method.save()


Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()


Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()


Create parties::

    >>> Subdivision = Model.get('country.subdivision')
    >>> Country = Model.get('country.country')
    >>> Party = Model.get('party.party')
    >>> CountryZip = Model.get('country.postal_code')
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> country_fr = Country(name="France", code="FR", eu_member=True)
    >>> country_fr.save()
    >>> paris = Subdivision(
    ...     name="Paris", code="FR-PA", type='autonomous community', country=country_fr)
    >>> paris.save()
    >>> address_fr = customer.addresses.new()
    >>> address_fr.name = 'Address FR'
    >>> address_fr.country = country_fr
    >>> address_fr.subdivision = paris
    >>> address_fr.save()
    >>> customer.save()

Get warehouse and add address::

    >>> Location = Model.get('stock.location')
    >>> Address = Model.get('party.address')
    >>> SubdivisionType = Model.get('party.address.subdivision_type')
    >>> wh, = Location.find([('type', '=', 'warehouse')])
    >>> country_es = Country(name="Spain", code="ES", eu_member=True)
    >>> country_es.save()
    >>> subdivision_type, = SubdivisionType.find([('country_code', '=', 'ES')])
    >>> subdivision_type.types = list(subdivision_type.types) + ['autonomous community']
    >>> subdivision_type.save()
    >>> rgmurcia = Subdivision(
    ...     name="Región de Murcia", code="ES-MC", type='autonomous community', country=country_es)
    >>> rgmurcia.save()
    >>> company_addr = company.party.addresses[0]
    >>> company_addr.country = country_es
    >>> company_addr.subdivision = rgmurcia
    >>> company_addr.save()
    >>> wh.address = company_addr
    >>> wh.save()


Configure Intrastat::

    >>> conf = configure_intrastat()
    >>> conf.declare_dispatch = True
    >>> conf.save()
    >>> TransactionType = Model.get('intrastat.transaction.type')
    >>> type11, = TransactionType.find([('code', '=', '11')])
    >>> conf.transaction_type = type11
    >>> conf.save()


Create Intrastat Declaration::

    >>> Declaration = Model.get('intrastat.declaration')
    >>> declaration = Declaration(from_date=today,to_date=today, company=company, description='Declaration 1')
    >>> declaration.save()


Create Intrastat codes::

    >>> Code = Model.get('intrastat.code')
    >>> code_c1 = Code(code='04', description="DAIRY PRODUCE; BIRDS' EGGS; NATURAL HONEY; EDIBLE PRODUCTS OF ANIMAL ORIGIN,  NOT ELSEWHERE SPECIFIED OR INCLUDED")
    >>> code_c1.save()
    >>> code_g1 = Code(code='0401', description='Milk and cream', parent=code_c1)
    >>> code_g1.save()
    >>> code_p1 = Code(code='0401 10', description='Of a fat content, by weight, not exceeding 1 %', parent=code_g1)
    >>> code_p1.save()
    >>> code_c1 = Code(code='0401 10 10', description='In immediate packings of a net content not exceeding two litres', parent=code_p1)
    >>> code_c1.save()


Create Intrastat Subdivisions distance::

    >>> SubdivisionDistance = Model.get('intrastat.subdivision_distance')
    >>> distance1 = SubdivisionDistance()
    >>> distance1.subdivision = rgmurcia
    >>> distance1.distance = 100
    >>> distance1.save()
    >>> distance2 = SubdivisionDistance()
    >>> distance2.subdivision = paris
    >>> distance2.distance = 100
    >>> distance2.save()


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')

    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.purchasable = True
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.account_category = account_category_tax
    >>> template.intrastat_code = code_c1
    >>> template.save()
    >>> product, = template.products
    
    >>> template2 = ProductTemplate()
    >>> template2.name = 'service'
    >>> template2.default_uom = unit
    >>> template2.type = 'service'
    >>> template2.salable = True
    >>> template2.list_price = Decimal('50')
    >>> template2.cost_price = Decimal('20')
    >>> template2.account_category = account_category_tax
    >>> template2.save()
    >>> service, = template2.products


Create carrier product::

    >>> carrier_template = ProductTemplate()
    >>> carrier_template.name = 'Carrier product'
    >>> carrier_template.type = 'service'
    >>> carrier_template.list_price = Decimal(500)
    >>> carrier_template.cost_price = Decimal(0)
    >>> carrier_template.default_uom = unit
    >>> carrier_template.save()
    >>> carrier_product, = carrier_template.products


Create carrier::

    >>> Carrier = Model.get('carrier')
    >>> party_carrier = Party(name='Carrier 1')
    >>> party_carrier.save()

    >>> carrier = Carrier()
    >>> carrier.party = party_carrier
    >>> carrier.carrier_product = carrier_product
    >>> carrier.save()


Create cost types::

    >>> CostType = Model.get('sale.cost.type')
    >>> type_carrier = CostType(name='Service')
    >>> type_carrier.product = service
    >>> type_carrier.formula = 'carrier_amount'
    >>> type_carrier.manual = True
    >>> type_carrier.save()


Create cost templates::

    >>> CostTemplate = Model.get('sale.cost.template')
    >>> template3 = CostTemplate()
    >>> template3.type_ = type_carrier
    >>> template3.save()


Create Sale::

    >>> Sale = Model.get('sale.sale')
    >>> address_fr, = [address for address in customer.addresses if address.country == country_fr]
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.shipment_address = address_fr
    >>> sale.invoice_address = address_fr
    >>> sale.invoice_method = 'order'
    >>> sale_line = sale.lines.new()
    >>> sale_line.product = product
    >>> sale_line.quantity = 2.0
    >>> sale.save()


Create load order::

    >>> Load = Model.get('carrier.load')
    >>> Order = Model.get('carrier.load.order')
    >>> load = Load()
    >>> load.warehouse = wh
    >>> load.carrier = carrier
    >>> load.vehicle_number = 'MX1212'
    >>> load.unit_price = Decimal('10')
    >>> load.save()

    >>> load_order = Order(load=load)
    >>> load_order.sale = sale
    >>> load_order.save()


Processing sale::

    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> shipment, = sale.shipments
    >>> shipment.planned_date = today
    >>> shipment.effective_date = today
    >>> shipment.save()
    >>> move, = [m for m in shipment.moves if m.origin == sale.lines[0]]
    >>> move.planned_date = today
    >>> move.effective_date = today
    >>> move.save()
    >>> shipment.click('wait')


Make 1 unit of the product available::

    >>> Move = Model.get('stock.move')
    >>> lost_found_loc, = Location.find([('type', '=', 'lost_found')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> incoming_move = Move()
    >>> incoming_move.product = product
    >>> incoming_move.uom = unit
    >>> incoming_move.quantity = 2
    >>> incoming_move.from_location = lost_found_loc
    >>> incoming_move.to_location = storage_loc
    >>> incoming_move.planned_date = today
    >>> incoming_move.effective_date = today
    >>> incoming_move.company = company
    >>> incoming_move.unit_price = Decimal('1')
    >>> incoming_move.currency = company.currency
    >>> incoming_move.click('do')


Do shipment::

    >>> shipment.click('assign_try')
    True
    >>> shipment.click('pick')
    >>> shipment.click('pack')
    >>> shipment.click('done')
    >>> sale.reload()
    >>> sale.click('confirm')
    >>> sale.click('process')
    >>> invoice, = sale.invoices
    >>> invoice.invoice_date = today
    >>> invoice.accounting_date = today
    >>> invoice.click('post')

Check statistic_value from IntrastatLine::

    >>> declaration.click('create_lines')
    >>> line, = declaration.lines
    >>> line.statistic_value
    Decimal('15.00')